# thgtoa-anonarchive-backup

GitLab backup of the offline formats of Version 1.2.2 of The Hitchhiker's Guide to Online Anonymity by Anonymous Planet. Pulled from anonarchive.org using public key `BPMWFUFLlN1obX3MA7RC?H2Ghz6A9hlNkanvT`.

## Access methods that still work [02/08/2022]
- [The Wayback Machine](https://web.archive.org/https://anonymousplanet.org)
- [AnonArchive](https://anonarchive.org) with public key `BPMWFUFLlN1obX3MA7RC?H2Ghz6A9hlNkanvT`
- [(Partially) CryptPad.fr](https://cryptpad.fr/drive/#/2/drive/view/Ughm9CjQJCwB8BIppdtvj5zy4PyE-8Gxn11x9zaqJLI/)
    - Many files are now inaccessible or require a password.

## Using/Verifying torrent

A .torrent file with magnet link `magnet:?xt=urn:btih:839dcfd23896ca961826120077326cdb24b29b0f&dn=thgtoa` has been added to help keep this guide accessible. It has the following SHA256 hash:

`043dca86f7661d3ade8d8e944eebf64251c8dd596599f1e8a9eb9661c423e97e`

Additionally, please feel free to [use these Virustotal results](https://www.virustotal.com/gui/file/043dca86f7661d3ade8d8e944eebf64251c8dd596599f1e8a9eb9661c423e97e?nocache=1) to confirm the hash, and re-upload the file yourself if need be.

The torrent itself has been signed using a GPG key with the following fingerprint:

`10E8 81D6 15A9 5E9D E45D  8A8F 6466 0786 C4D1 951C`

It has been exclusively generated for this repository. The private key was destroyed after signing this torrent; the public key and digest are included in the repository itself to be verified.

If you have the opportunity after downloading these files, please feel free to use the torrent/magnet link to seed them to others; additionally, feel free to upload it to any trackers that will accept it.

## Licensing

The initial guide is [licensed](https://web.archive.org/anonymousplanet.org/LICENSE.html) under [Creative Commons Attribution-NonCommerical 4.0 International](https://web.archive.org/web/20220303135123/https://creativecommons.org/licenses/by-nc/4.0/) - and by extension, so is this repository.
